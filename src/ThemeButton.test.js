import { render, screen } from "@testing-library/react";
import userEvent from "@testing-library/user-event";
import ThemeButton from "./ThemeButton";

test("Test theme button toggle", () => {
    render(<ThemeButton />);
    const buttonElm = screen.getByText(/current theme/i);

    // first click
    userEvent.click(buttonElm);
    expect(buttonElm).toHaveTextContent(/dark/i);

    // second click
    userEvent.click(buttonElm);
    expect(buttonElm).toHaveTextContent(/light/i);
});
